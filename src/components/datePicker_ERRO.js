import react, { useState } from "react";
import { View, Button } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";

const DateTimePicker = ({ type, buttonTitlle, dateKey, setSchedule }) => {
  const [datePickerVisible, setDatePickerVisible] = useState(false);

  //constantes para alterar o estador
  const showDatePicker = () => {
    setDatePickerVisible(true);
  };

  //fechar
  const hideDatePicker = () => {
    setDatePickerVisible(false);
  };

  const handleConfirm = (date) => {
    if (type === "time") {
      //Lógica para extrair hora e minuto
      const hour = date.getHours();
      const minute = date.getMinuts();

      //Lógica para montar hora e minuto no formato desejado
      const formattedTime = `${hour}:${minute}`;

      //Atualiza o estado da reserva com HH:mm formatada
      setSchedule((prevState) => ({
        ...prevState,
        [dateKey]: formattedTime,
      }));
    } else {
      //formatando a data,split corta o T 
      const formattedDate = date.toISOString().split('T')[0];
      //Atualizar Schedule
      setSchedule((prevState) => ({
        ...prevState,
        [dateKey]: formattedDate,
      }));
      // datekey= chave dentro do schedule(objeto com atributos),pega esse valor e converte como se tivesse usando a logica de objetos
      //object.objectkeys=captura as chaves do objeto
    }
    hideDatePicker();
  };

  return (
    <View>
      <Button title={buttonTitlle} onPress={showDatePicker} color="#588" />
      <DateTimePickerModal
        isVisible={datePickerVisible}
        mode={type}
        locale="pt_BR"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />
    </View>
  );
};

export default DateTimePicker;
